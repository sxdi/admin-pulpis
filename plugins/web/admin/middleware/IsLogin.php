<?php namespace Web\Admin\Middleware;

use Redirect;

use Closure;

use Pulangpisau\Core\Classes\SessionManager;

class IsLogin
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $session = new SessionManager();
        $user    = $session->get();

        if(!$user) {
            return Redirect::to('/?callback='.$request->fullUrl());
        }

        return $next($request);
    }
}
