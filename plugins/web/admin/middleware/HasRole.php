<?php namespace Web\Admin\Middleware;

use Redirect;

use Closure;

use Pulangpisau\Core\Classes\SessionManager;

class HasRole
{
    /**
     * Run the request filter.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $session   = new SessionManager();
        $user      = $session->get();
        $kkm       = explode('|', $role);
        $hasAccess = $user->hasAccess($kkm);

        if($hasAccess) {
            return $next($request);
        }

        return Redirect::back();
    }
}
