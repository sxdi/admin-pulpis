<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Core\Classes\SessionManager;
use Pulangpisau\Core\Classes\RequestPermitManager;

use Pulangpisau\Location\Models\Province;
use Pulangpisau\Location\Models\Regency;
use Pulangpisau\Location\Models\District;
use Pulangpisau\Location\Models\Village;

use Pulangpisau\Service\Models\Service as ServiceModels;

use Pulangpisau\Request\Models\Request as RequestModels;
use Pulangpisau\Request\Models\RequestItem as RequestItemModels;

use Pulangpisau\Request\Models\RequestHearsePermit;

class AdminRequestPermitHearse extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminRequestPermitHearse Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }

    public function getService($id)
    {
        return ServiceModels::whereId($id)->first();
    }

    public function onSelectOriginRegency()
    {
        $district                       = District::whereRegencyId(post('origin_regency'))->orderBy('name', 'asc')->get();
        $this->page['origin_districts'] = $district;
    }

    public function onSelectDestinationDistrict()
    {
        $village                            = Village::whereDistrictId(post('destination_district'))->orderBy('name', 'asc')->get();
        $this->page['destination_villages'] = $village;
    }

    public function onSelectDestinationRegency()
    {
        $district                            = District::whereRegencyId(post('destination_regency'))->orderBy('name', 'asc')->get();
        $this->page['destination_districts'] = $district;
    }

    public function onSelectOriginDistrict()
    {
        $village                       = Village::whereDistrictId(post('origin_district'))->orderBy('name', 'asc')->get();
        $this->page['origin_villages'] = $village;
    }

    public function onSave()
    {
        $user          = $this->getUser();
        $permitManager = new RequestPermitManager();

        $rules = [
            'name'                 => 'required',
            'phone'                => 'required',
            'email'                => 'email',

            'date'                 => 'required|date',
            'origin_regency'       => 'required',
            'origin_district'      => 'required',
            'origin_village'       => 'required',
            'origin_rt'            => 'required',
            'origin_rw'            => 'required',
            'origin_address'       => 'required',
            'destination_regency'  => 'required',
            'destination_district' => 'required',
            'destination_village'  => 'required',
            'destination_rt'       => 'required',
            'destination_rw'       => 'required',
            'destination_address'  => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'name'                 => 'nama',
            'phone'                => 'no telepon',
            'email'                => 'email',

            'date'                 => 'tanggal pengawalan',
            'origin_regency'       => 'asal kabupaten',
            'origin_district'      => 'asal kecamatan',
            'origin_village'       => 'asal kelurahan',
            'origin_rt'            => 'asal rt',
            'origin_rw'            => 'asal rw',
            'origin_address'       => 'asal alamat',
            'destination_regency'  => 'tujuan kabupaten',
            'destination_district' => 'tujuan kecamatan',
            'destination_village'  => 'tujuan kelurahan',
            'destination_rt'       => 'tujuan rt',
            'destination_rw'       => 'tujuan rw',
            'destination_address'  => 'tujuan alamat',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $request = RequestModels::firstOrCreate([
            'code' => input('origin_code')
        ]);
        $request->save();

        $requestItem                   = new RequestItemModels;
        $requestItem->request_id       = $request->id;
        $requestItem->requestable_type = $service->model;
        $requestItem->service_id       = $service->id;
        $requestItem->service_name     = $service->name;
        $requestItem->user_id          = $user ? $user->id : '';
        $requestItem->status           = 'request';
        $requestItem->save();

        $permit                          = new RequestHearsePermit;
        $permit->item_id                 = $requestItem->id;
        $permit->schedule_at             = post('date');
        $permit->origin_regency_id       = post('origin_regency');
        $permit->origin_district_id      = post('origin_district');
        $permit->origin_village_id       = post('origin_village');
        $permit->origin_rt               = post('origin_rt');
        $permit->origin_rw               = post('origin_rw');
        $permit->origin_address          = post('origin_address');
        $permit->destination_regency_id  = post('destination_regency');
        $permit->destination_district_id = post('destination_district');
        $permit->destination_village_id  = post('destination_village');
        $permit->destination_rt          = post('destination_rt');
        $permit->destination_rw          = post('destination_rw');
        $permit->destination_address     = post('destination_address');
        $permit->save();

        $requestItem->requestable_id = $permit->id;
        $requestItem->save();

        /**
         * If reference
         */
        if(input('ref')) {
            $permitManager->makeQueue($requestItem->id, input('ref'));
        }

        /**
         * Request Customer
         * @var RequestCustomer
         */
        $permitManager->makeRequester($requestItem->id, post());

        /**
         * Request MakeOfficer
         * @var RequestMakeOfficer
         */
        $permitManager->makeOfficer($requestItem->id, post());

        Flash::success('Permohonan berhasil disimpan');
        return Redirect::to('permohonan/item/detail/'.$requestItem->parameter);
    }
}
