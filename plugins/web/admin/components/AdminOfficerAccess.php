<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\User\Models\User as UserModels;
use Pulangpisau\User\Models\UserGroup as UserGroupModels;
use Pulangpisau\User\Models\UserUserGroup as UserUserGroupModels;

class AdminOfficerAccess extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminOfficerAccess Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $user = $this->getCurrent();
        if(!$user) {
            Flash::error('Data tidak ditemukan');
            return Redirect::to('404');
        }

        $this->page->title    = 'Hak akses : '.$user->code;

        $this->page['groups'] = $this->getAccess();
        $this->page['user']   = $user;
    }

    public function getCurrent()
    {
        return UserModels::whereParameter($this->property('parameter'))->first();
    }

    public function getAccess()
    {
        return UserGroupModels::orderBy('name')->get();
    }

    public function onSave()
    {
        $user = $this->getCurrent();
        for ($i=0; $i < count(post('group_id')); $i++) {
            if(post('parameter')[$i]) {
                if(isset(post('is_access')[$i])) {
                    UserUserGroupModels::whereParameter(post('parameter')[$i])->update([
                        'group_id' => post('group_id')[$i]
                    ]);
                }
                else {
                    UserUserGroupModels::whereParameter(post('parameter')[$i])->delete();
                }
            }
            else {
                if(isset(post('is_access')[$i])) {
                    $k           = new UserUserGroupModels;
                    $k->user_id  = $user->id;
                    $k->group_id = post('group_id')[$i];
                    $k->save();
                }
            }

            Flash::success('Hak akses berhasil disimpan');
            return Redirect::refresh();
        }
    }
}
