<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Core\Classes\SessionManager;
use Pulangpisau\Core\Classes\RequestPermitManager;

use Pulangpisau\Service\Models\Service as ServiceModels;

use Pulangpisau\Request\Models\Request as RequestModels;
use Pulangpisau\Request\Models\RequestItem as RequestItemModels;

use Pulangpisau\Request\Models\RequestActivityPermit;

class AdminRequestPermitActivity extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminRequestPermitActivity Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }

    public function getService($id)
    {
        return ServiceModels::whereId($id)->first();
    }

    public function onSave()
    {
        $user          = $this->getUser();
        $service       = $this->getService(post('service_id'));
        $permitManager = new RequestPermitManager();

        $rules = [
            'name'                        => 'required',
            'phone'                       => 'required',
            'email'                       => 'email',
            'gender'                      => 'required|in:male,female',
            'job'                         => 'required',
            'nationality'                 => 'required',
            'address'                     => 'required',

            'hasPermission'               => 'required|in:1',
            'hasProposal'                 => 'required|in:1',
            'hasRecommendationPlace'      => 'required|in:1',
            'hasRecommendationInstantion' => 'required|in:1',
            'hasRecommendationPolice'     => 'required|in:1',
            'hasArt'                      => 'required|in:1',
        ];
        $messages       = [];
        $attributeNames = [
            'name'                        => 'nama',
            'phone'                       => 'no telepon',
            'email'                       => 'email',
            'gender'                      => 'jenis kelamin',
            'job'                         => 'pekerjaan',
            'nationality'                 => 'kebangsaan',
            'address'                     => 'alamat',

            'hasPermission'               => 'berkas permohonan ketua pelaksana',
            'hasProposal'                 => 'berkas proposal kegiatan',
            'hasRecommendationPlace'      => 'berkas perizinan tempat',
            'hasRecommendationInstantion' => 'berkas permohonan instansi terkait',
            'hasRecommendationPolice'     => 'berkas permohonan polsek/polda setempat',
            'hasArt'                      => 'berkas AD/ART',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        /**
         * Request
         * @var [type]
         */
        $request = RequestModels::firstOrCreate([
            'code' => input('origin_code')
        ]);
        $request->save();

        /**
         * Request Item
         * @var RequestItemModels
         */
        $requestItem                   = new RequestItemModels;
        $requestItem->request_id       = $request->id;
        $requestItem->requestable_type = $service->parent->model;
        $requestItem->service_id       = $service->id;
        $requestItem->service_name     = $service->name;
        $requestItem->user_id          = $user ? $user->id : '';
        $requestItem->status           = 'progress';
        $requestItem->save();

        /**
         * Request Permit
         * @var RequestActivityPermit
         */
        $permit                              = new RequestActivityPermit;
        $permit->item_id                     = $requestItem->id;
        $permit->hasPermission               = post('hasPermission');
        $permit->hasProposal                 = post('hasProposal');
        $permit->hasRecommendationPlace      = post('hasRecommendationPlace');
        $permit->hasRecommendationInstantion = post('hasRecommendationInstantion');
        $permit->hasRecommendationPolice     = post('hasRecommendationPolice');
        $permit->hasArt                      = post('hasArt');
        $permit->hasKtp                      = post('hasKtp');
        $permit->save();

        $requestItem->requestable_id = $permit->id;
        $requestItem->save();

         /**
         * If reference
         */
        if(input('ref')) {
            $permitManager->makeQueue($requestItem->id, input('ref'));
        }

        /**
         * Request Customer
         * @var RequestCustomer
         */
        $permitManager->makeRequester($requestItem->id, post());

        /**
         * Request Officer
         * @var RequestOfficer
         */
        $permitManager->makeOfficer($requestItem->id, post());

        Flash::success('Permohonan berhasil disimpan');
        return Redirect::to('permohonan/item/detail/'.$requestItem->parameter);
    }
}
