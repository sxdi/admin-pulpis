<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

use Renatio\DynamicPDF\Classes\PDF;

use Pulangpisau\Request\Models\RequestFeedback as RequestFeedbackModels;

class AdminFeedbackFile extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminFeedbackFile Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $feedback = $this->getCurrent();
        if(!$feedback) {

        }

        $templateCode = 'pulangpisau::feedback'; // unique code of the template
        $data         = [
            'feedback' => $feedback
        ]; // optional data used in template
        return PDF::loadTemplate($templateCode, $data)->stream($feedback->code.'.pdf');
    }

    public function getCurrent()
    {
        return RequestFeedbackModels::whereParameter($this->property('parameter'))->first();
    }
}
