<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

use Pulangpisau\Request\Models\Request as RequestModels;

class AdminRequestDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminRequestDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $request = $this->getCurrent();

        $this->page->title     = ucwords('Permohonan Kode:'.$request->code);
        $this->page['request'] = $request;
    }

    public function getCurrent()
    {
        return RequestModels::whereParameter($this->property('parameter'))->first();
    }
}
