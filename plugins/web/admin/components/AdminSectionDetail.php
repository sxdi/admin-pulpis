<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Section\Models\Section as SectionModels;

class AdminSectionDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminSectionDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $section = $this->getCurrent();

        $this->page['section'] = $section;
    }

    public function getCurrent()
    {
        return SectionModels::whereParameter($this->property('parameter'))->first();
    }

    public function onSave()
    {
        $rules = [
            'name'        => 'required',
            'code'        => 'required|unique:pulangpisau_section_sections,code,'.$this->getCurrent()->id,
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'code'        => 'kode',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $counter              = $this->getCurrent();
        $counter->name        = post('name');
        $counter->code        = post('code');
        $counter->description = post('description');
        $counter->save();

        Flash::success('Bagian petugas berhasil disimpan');
        return Redirect::refresh();
    }
}
