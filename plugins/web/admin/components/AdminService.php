<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Service\Models\Service as ServiceModels;

class AdminService extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminService Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return $this->getParent();
        return ServiceModels::orderBy('name')->get();
    }

    public function getParent()
    {
        return ServiceModels::whereNull('parent_id')->orderBy('name')->get();
    }

    public function getServiceById($id)
    {
        return ServiceModels::whereId($id)->first();
    }

    public function onSelectParent()
    {
        if(post('parent_id') == 'new') {
            $this->page['is_parent'] = false;
            return;
        }

        $this->page['is_parent'] = true;
    }

    public function onSave()
    {
        $rules = [
            'parent_id'   => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'parent_id'   => 'turunan'

        ];

        if(post('parent_id') == 'new') {
            $rules['name']                 = 'required';
            $attributeNames['name']        = 'nama';
            $rules['description']          = 'required';
            $attributeNames['description'] = 'konten';
            $rules['day_open']             = 'required';
            $attributeNames['day_open']    = 'konten';
            $rules['time_open']            = 'required';
            $attributeNames['time_open']   = 'konten';
        }
        else {
            $rules['name']                 = 'required';
            $attributeNames['name']        = 'nama';
        }

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $service              = new ServiceModels;

        if(post('parent_id') == 'new') {
            $service->name        = post('name');
            $service->description = post('description');
            $service->day_open    = post('day_open');
            $service->time_open   = post('time_open');
        }
        else {
            $s                    = $this->getServiceById(post('parent_id'));
            $service->parent_id   = $s->id;
            $service->name        = post('name');
            $service->description = $s->description;
            $service->day_open    = $s->day_open;
            $service->time_open   = $s->time_open;
            $service->save(null, post('_session_key'));
        }

        $service->save(null, post('_session_key'));

        Flash::success('Layanan berhasil disimpan');
        return Redirect::refresh();
    }
}
