<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

class AdminOfficerDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminOfficerDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }
}
