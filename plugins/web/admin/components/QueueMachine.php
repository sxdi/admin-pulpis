<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

use Pulangpisau\Counter\Models\Counter;

class QueueMachine extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'QueueMachine Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $this->page['counters'] = $this->getAll();
    }

    public function getAll()
    {
        return Counter::orderBy('id')->get();
    }
}
