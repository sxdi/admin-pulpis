<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Sector\Models\Sector as SectorModels;

class AdminSectorDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminSectorDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $sector = $this->getCurrent();

        $this->page['sector'] = $sector;
    }

    public function getCurrent()
    {
        return SectorModels::whereParameter($this->property('parameter'))->first();
    }


    /**
     * Action
     * @return [type] [description]
     */
    public function onSave()
    {
        $rules = [
            'name'        => 'required',
            'province_id' => 'required',
            'regency_id'  => 'required',
            'district_id' => 'required',
            'village_id'  => 'required',
            'address'     => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'province_id' => 'provinsi',
            'regency_id'  => 'kota/kabupaten',
            'district_id' => 'kecamatan',
            'village_id'  => 'kelurahan',
            'address'     => 'alamat lengkap',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $sector              = $this->getCurrent();
        $sector->name        = post('name');
        $sector->province_id = post('province_id');
        $sector->regency_id  = post('regency_id');
        $sector->district_id = post('district_id');
        $sector->village_id  = post('village_id');
        $sector->address     = post('address');
        $sector->save();

        Flash::success('Sektor berhasil disimpan');
        return Redirect::refresh();
    }
}
