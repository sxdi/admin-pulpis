<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Section\Models\Section as SectionModels;

class AdminSection extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminSection Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return SectionModels::orderBy('name')->get();
    }

    public function onSave()
    {
        $rules = [
            'name'        => 'required',
            'code'        => 'required|unique:pulangpisau_unit_units,code',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'code'        => 'kode',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $counter              = new SectionModels;
        $counter->name        = post('name');
        $counter->code        = post('code');
        $counter->description = post('description');
        $counter->save();

        Flash::success('Bagian petugas berhasil disimpan');
        return Redirect::refresh();
    }
}
