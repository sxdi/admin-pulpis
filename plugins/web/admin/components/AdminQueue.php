<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Queue\Models\Queue as QueueModels;

class AdminQueue extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminQueue Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return QueueModels::orderBy('created_at', 'desc')->groupBy('created_at')->get();
    }
}
