<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

use Pulangpisau\Request\Models\RequestFeedback as RequestFeedbackModels;

class AdminFeedbackDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminFeedbackDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $feedback = $this->getCurrent();
        if(!$feedback) {

        }


        $this->page->title      = 'Penilaian permohonan '.$feedback->item->request->code;
        $this->page['feedback'] = $feedback;
    }

    public function getCurrent()
    {
        return RequestFeedbackModels::whereParameter($this->property('parameter'))->first();
    }
}
