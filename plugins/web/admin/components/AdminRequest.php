<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Officer\Models\Officer as OfficerModels;
use Pulangpisau\Service\Models\Service as ServiceModels;

use Pulangpisau\Location\Models\Regency;

use Pulangpisau\Core\Classes\SessionManager;
use Pulangpisau\Core\Classes\RequestPermitManager;

use Pulangpisau\Queue\Models\Queue as QueueModels;

use Pulangpisau\Request\Models\Request as RequestModels;
use Pulangpisau\Request\Models\RequestItem as RequestItemModels;

use Pulangpisau\User\Models\UserService as UserServiceModels;

use Pulangpisau\Request\Models\RequestSim as RequestSimModels;
use Pulangpisau\Request\Models\RequestSimStudent as RequestSimStudentModels;
use Pulangpisau\Request\Models\RequestSkck as RequestSkckModels;
use Pulangpisau\Request\Models\RequestTnkbYearly as RequestTnkbYearlyModels;
use Pulangpisau\Request\Models\RequestTnkbRenewal as RequestTnkbRenewalModels;
use Pulangpisau\Request\Models\RequestTnkbLost as RequestTnkbLostModels;
use Pulangpisau\Request\Models\RequestTnkbReunited as RequestTnkbReunitedModels;
use Pulangpisau\Request\Models\RequestTnkbOut as RequestTnkbOutModels;
use Pulangpisau\Request\Models\RequestTnkbIn as RequestTnkbInModels;
use Pulangpisau\Request\Models\RequestCrowd as RequestCrowdModels;
use Pulangpisau\Request\Models\RequestCrowdFirework as RequestCrowdFireworkModels;
use Pulangpisau\Request\Models\RequestCrowdPublic as RequestCrowdPublicModels;
use Pulangpisau\Request\Models\RequestCrowdCampaign as RequestCrowdCampaignModels;
use Pulangpisau\Request\Models\RequestHearse as RequestHearseModels;
use Pulangpisau\Request\Models\RequestHearseOther as RequestHearseOtherModels;
use Pulangpisau\Request\Models\RequestDrug as RequestDrugModels;
use Pulangpisau\Request\Models\RequestFinger as RequestFingerModels;
use Pulangpisau\Request\Models\RequestLost as RequestLostModels;
use Pulangpisau\Request\Models\RequestPickSick as RequestPickSickModels;

class AdminRequest extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminRequest Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onRun()
    {
        $queue         = QueueModels::whereParameter(input('ref'))->first();

        if($queue) {
            $kk = ServiceModels::whereHas('counters', function($q) use($queue) {
                $q->whereCounterId($queue->counter_id);
            })->first();
            $this->page['services'] = $kk->childs;
        }
    }

    public function getService()
    {
        return ServiceModels::orderBy('name')->get();
    }

    public function getServiceById($id)
    {
        return ServiceModels::whereId($id)->first();
    }

    public function getOfficer()
    {
        return OfficerModels::has('user')->orderBy('name')->get();
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }

    public function getAllRequest()
    {
        $user     = $this->getUser();
        $services = [];

        foreach ($user->services as $service) {
            foreach ($service->service->childs as $child) {
                array_push($services, $child->id);
            }
        }

        return RequestModels::orderBy('created_at', 'desc')->whereHas('items', function($q) use($services){
            $q->whereIn('service_id', $services);
            $q->whereIsDisplay(1);
        })->get();
    }

    public function getAll()
    {
        return RequestModels::orderBy('created_at', 'desc')->get();
    }


    /**
     * Action
     * @return [type] [description]
     */
    public function onSave()
    {
        $user          = $this->getUser();
        $service       = $this->getServiceById(post('service_id'));
        $permitManager = new RequestPermitManager();

        $rules = [
            'name'        => 'required',
            'phone'       => 'required',
            'email'       => 'email',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'phone'       => 'no telepon',
            'email'       => 'email',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        /**
         * Request
         * @var [type]
         */
        $request = RequestModels::firstOrCreate([
            'code' => input('origin_code')
        ]);
        $request->save();

        /**
         * Request Item
         * @var RequestItemModels
         */
        $requestItem                   = new RequestItemModels;
        $requestItem->request_id       = $request->id;
        $requestItem->requestable_type = $service->parent->model;
        $requestItem->service_id       = $service->id;
        $requestItem->service_name     = $service->name;
        $requestItem->user_id          = $user ? $user->id : '';
        $requestItem->status           = 'finish';
        $requestItem->save();


        /**
         * Sim Table
         */
        if($service->parent->code == 'permit-sim') {
            if($service->code == 'sim-new') {
                $sim                    = new RequestSimModels;
                $sim->item_id           = $requestItem->id;
                $sim->type              = post('kind');
                $sim->hasGlasses        = post('hasGlasses');
                $sim->hasPsy            = post('hasPsy');
                $sim->hasCertified      = post('hasCertified');
                $sim->height            = post('height');
                $sim->education         = post('education');
                $sim->father            = post('father');
                $sim->mother            = post('mother');
                $sim->address_city      = post('address_city');
                $sim->address_village   = post('address_village');
                $sim->address_rt        = post('address_rt');
                $sim->address_rw        = post('address_rw');
                $sim->address_pos       = post('address_pos');
                $sim->emergency_city    = post('emergency_city');
                $sim->emergency_village = post('emergency_village');
                $sim->emergency_rt      = post('emergency_rt');
                $sim->emergency_rw      = post('emergency_rw');
                $sim->emergency_pos     = post('emergency_pos');
                $sim->save();
            }

            if($service->code == 'sim-extend') {
                $sim                    = new RequestSimModels;
                $sim->item_id           = $requestItem->id;
                $sim->type              = post('kind');
                $sim->hasGlasses        = post('hasGlasses');
                $sim->hasPsy            = post('hasPsy');
                $sim->hasCertified      = post('hasCertified');
                $sim->height            = post('height');
                $sim->education         = post('education');
                $sim->father            = post('father');
                $sim->mother            = post('mother');
                $sim->address_city      = post('address_city');
                $sim->address_village   = post('address_village');
                $sim->address_rt        = post('address_rt');
                $sim->address_rw        = post('address_rw');
                $sim->address_pos       = post('address_pos');
                $sim->emergency_city    = post('emergency_city');
                $sim->emergency_village = post('emergency_village');
                $sim->emergency_rt      = post('emergency_rt');
                $sim->emergency_rw      = post('emergency_rw');
                $sim->emergency_pos     = post('emergency_pos');
                $sim->save();
            }

            if($service->code == 'sim-student') {
                $sim                    = new RequestSimStudentModels;
                $sim->item_id           = $requestItem->id;
                $sim->type              = post('kind');
                $sim->hasGlasses        = post('hasGlasses');
                $sim->hasPsy            = post('hasPsy');
                $sim->hasCertified      = post('hasCertified');
                $sim->height            = post('height');
                $sim->education         = post('education');
                $sim->father            = post('father');
                $sim->mother            = post('mother');
                $sim->address_city      = post('address_city');
                $sim->address_village   = post('address_village');
                $sim->address_rt        = post('address_rt');
                $sim->address_rw        = post('address_rw');
                $sim->address_pos       = post('address_pos');
                $sim->emergency_city    = post('emergency_city');
                $sim->emergency_village = post('emergency_village');
                $sim->emergency_rt      = post('emergency_rt');
                $sim->emergency_rw      = post('emergency_rw');
                $sim->emergency_pos     = post('emergency_pos');
                $sim->save();
            }

            $requestItem->requestable_id = $sim->id;
            $requestItem->save();
        }

        /**
         * Skck Table
         */
        if($service->parent->code == 'permit-skck') {
            $skck                                  = new RequestSkckModels;
            $skck->item_id                         = $requestItem->id;
            $skck->hasSkck                         = 'false';
            $skck->hasFinger                       = 'false';
            $skck->hasPaspor                       = post('hasPaspor');

            $skck->is_criminal                     = post('is_criminal');
            if(post('is_criminal')                 == 'true') {
                $skck->criminal_case                   = post('criminal_case');
                $skck->criminal_punishment             = post('criminal_punishment');
                $skck->criminal_punishment_process     = post('criminal_punishment_process');
                $skck->criminal_punishment_description = post('criminal_punishment_description');
            }

            $skck->is_violation                    = post('is_violation');
            if(post('is_violation')                == 'true') {
                $skck->violation_case                  = post('violation_case');
                $skck->vionlation_punishment           = post('vionlation_punishment');
            }

            if(post('is_sponsor')                  == 'true') {
                $skck->sponsor                         = post('sponsor');
                $skck->sponsor_address                 = post('sponsor_address');
                $skck->sponsor_phone                   = post('sponsor_phone');
                $skck->sponsor_business                = post('sponsor_business');
            }

            $skck->purpose                         = post('purpose');
            $skck->job_description                 = post('job_description');
            $skck->hobby                           = post('hobby');
            $skck->address                         = post('phone_address');

            if(post('is_marriage')) {
                $skck->relation                        = post('is_marriage');
                $skck->relation_name                   = post('relation_name');
                $skck->relation_age                    = post('relation_age');
                $skck->relation_religion               = post('relation_religion');
                $skck->relation_nationality            = post('relation_nationality');
                $skck->relation_job                    = post('relation_job');
                $skck->relation_address                = post('relation_address');
            }

            $skck->father_name                     = post('father_name');
            $skck->father_age                      = post('father_age');
            $skck->father_religion                 = post('father_religion');
            $skck->father_nationality              = post('father_nationality');
            $skck->father_job                      = post('father_job');
            $skck->father_address                  = post('father_address');

            $skck->mother_name                     = post('mother_name');
            $skck->mother_age                      = post('mother_age');
            $skck->mother_religion                 = post('mother_religion');
            $skck->mother_nationality              = post('mother_nationality');
            $skck->mother_job                      = post('mother_job');
            $skck->mother_address                  = post('mother_address');

            $skck->siblings0name                   = post('siblings0name');
            $skck->siblings0age                    = post('siblings0age');
            $skck->siblings0job                    = post('siblings0job');
            $skck->siblings0address                = post('siblings0address');
            $skck->siblings1name                   = post('siblings1name');
            $skck->siblings1age                    = post('siblings1age');
            $skck->siblings1job                    = post('siblings1job');
            $skck->siblings1address                = post('siblings1address');
            $skck->siblings2name                   = post('siblings2name');
            $skck->siblings2age                    = post('siblings2age');
            $skck->siblings2job                    = post('siblings2job');
            $skck->siblings2address                = post('siblings2address');
            $skck->siblings3name                   = post('siblings3name');
            $skck->siblings3age                    = post('siblings3age');
            $skck->siblings3job                    = post('siblings3job');
            $skck->siblings3address                = post('siblings3address');

            $skck->schools0name                    = post('schools0name');
            $skck->schools0year                    = post('schools0year');
            $skck->schools1name                    = post('schools1name');
            $skck->schools1year                    = post('schools1year');
            $skck->schools2name                    = post('schools2name');
            $skck->schools2year                    = post('schools2year');
            $skck->schools3name                    = post('schools3name');
            $skck->schools3year                    = post('schools3year');

            $skck->save();

            $requestItem->requestable_id = $skck->id;
            $requestItem->save();
        }


        /**
         * Tnkb Table
        */
        if($service->parent->code == 'permit-tnkb') {
            if($service->code == 'tnkb-yearly') {
                $tnkb          = new RequestTnkbYearlyModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }

            if($service->code == 'tnkb-renewal') {
                $tnkb          = new RequestTnkbRenewalModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }

            if($service->code == 'tnkb-lost') {
                $tnkb          = new RequestTnkbLostModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }

            if($service->code == 'tnkb-reunited') {
                $tnkb          = new RequestTnkbReunitedModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }

            if($service->code == 'tnkb-out') {
                $tnkb          = new RequestTnkbOutModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }

            if($service->code == 'tnkb-in') {
                $tnkb          = new RequestTnkbInModels;
                $tnkb->item_id = $requestItem->id;
                $tnkb->save();

                $requestItem->requestable_id = $tnkb->id;
                $requestItem->save();
            }
        }

        /**
         * Crowd Table
        */
        if($service->parent->code == 'permit-crowd') {
            if($service->code == 'crowd') {
                $crowd              = new RequestCrowdModels;
                $crowd->item_id     = $requestItem->id;
                $crowd->kind        = post('kind');
                $crowd->place       = post('place');
                $crowd->participant = post('participant');
                $crowd->save();

                $requestItem->requestable_id = $crowd->id;
                $requestItem->save();
            }

            if($service->code == 'crowd-firework') {
                $crowd          = new RequestCrowdFireworkModels;
                $crowd->item_id = $requestItem->id;
                $crowd->purpose = post('purpose');
                $crowd->qty     = post('qty');
                $crowd->source  = post('source');
                $crowd->kind    = post('kind');
                $crowd->date    = post('date');
                $crowd->time    = post('time');
                $crowd->pic     = post('pic');
                $crowd->save();

                $requestItem->requestable_id = $crowd->id;
                $requestItem->save();
            }

            if($service->code == 'crowd-public') {
                $crowd              = new RequestCrowdPublicModels;
                $crowd->item_id     = $requestItem->id;
                $crowd->kind        = post('kind');
                $crowd->date        = post('date');
                $crowd->time        = post('time');
                $crowd->place       = post('place');
                $crowd->save();

                $requestItem->requestable_id = $crowd->id;
                $requestItem->save();
            }

            if($service->code == 'crowd-campaign') {
                $crowd                   = new RequestCrowdCampaignModels;
                $crowd->item_id          = $requestItem->id;
                $crowd->campaign_group   = post('campaign_group');
                $crowd->campaign_address = post('campaign_address');
                $crowd->kind             = post('kind');
                $crowd->date             = post('date');
                $crowd->time             = post('time');
                $crowd->place            = post('place');
                $crowd->participant      = post('participant');
                $crowd->lead             = post('lead');
                $crowd->vehicle          = post('vehicle');
                $crowd->apk              = post('apk');
                $crowd->save();

                $requestItem->requestable_id = $crowd->id;
                $requestItem->save();
            }
        }

        /**
         * Hearse Table
        */
        if($service->parent->code == 'permit-hearse') {
            if($service->code == 'hearse') {
                $hearse                          = new RequestHearseModels;
                $hearse->item_id                 = $requestItem->id;
                $hearse->scheduled_at            = Carbon::parse(post('appointment'))->format('Y-m-d H:i:s');
                $hearse->origin_regency_id       = post('origin_regency_id');
                $hearse->origin_district_id      = post('origin_district_id');
                $hearse->origin_village_id       = post('origin_village_id');
                $hearse->origin_rt               = post('origin_rt');
                $hearse->origin_rw               = post('origin_rw');
                $hearse->origin_address          = post('origin_address');
                $hearse->destination_regency_id  = post('destination_regency_id');
                $hearse->destination_district_id = post('destination_district_id');
                $hearse->destination_village_id  = post('destination_village_id');
                $hearse->destination_rt          = post('destination_rt');
                $hearse->destination_rw          = post('destination_rw');
                $hearse->destination_address     = post('destination_address');
                $hearse->save();

                $requestItem->requestable_id = $hearse->id;
                $requestItem->save();
            }

            if($service->code == 'hearse-other') {
                $hearse                      = new RequestHearseOtherModels;
                $hearse->item_id             = $requestItem->id;
                $hearse->scheduled_at        = Carbon::parse(post('appointment'))->format('Y-m-d H:i:s');

                $hearse->type                = post('hearse_type');
                $hearse->origin_address      = post('origin_address');
                $hearse->origin_rt           = post('origin_rt');
                $hearse->origin_rw           = post('origin_rw');
                $hearse->destination_address = post('destination_address');
                $hearse->destination_rt      = post('destination_rt');
                $hearse->destination_rw      = post('destination_rw');
                $hearse->save();

                $requestItem->requestable_id = $hearse->id;
                $requestItem->save();
            }
        }

        /**
         * Drug Table
        */
        if($service->parent->code == 'permit-drug') {
            if($service->code == 'drug-new') {
                $drug               = new RequestDrugModels;
                $drug->item_id      = $requestItem->id;
                $drug->header_doctor= post('header_doctor');
                $drug->purpose      = post('purpose');
                $drug->save();

                $requestItem->requestable_id = $drug->id;
                $requestItem->save();
            }

            if($service->code == 'drug-extend') {
                $drug               = new RequestDrugModels;
                $drug->item_id      = $requestItem->id;
                $drug->header_doctor= post('header_doctor');
                $drug->purpose      = post('purpose');
                $drug->save();

                $requestItem->requestable_id = $drug->id;
                $requestItem->save();
            }
        }

        /**
         * Finger Table
        */
        if($service->parent->code == 'permit-finger') {
            if($service->code == 'finger-new') {
                $finger          = new RequestFingerModels;
                $finger->item_id = $requestItem->id;
                $finger->save();

                $requestItem->requestable_id = $finger->id;
                $requestItem->save();
            }
        }

        /**
         * Lost Table
        */
        if($service->parent->code == 'permit-lost') {
            if($service->code == 'lost-new') {
                $lost              = new RequestLostModels;
                $lost->item_id     = $requestItem->id;
                $lost->kind        = post('kind');
                $lost->content     = post('content');
                $lost->date        = post('date');
                $lost->time        = post('time');
                $lost->place       = post('place');
                $lost->description = post('description');
                $lost->save();

                $requestItem->requestable_id = $lost->id;
                $requestItem->save();
            }
        }

        /**
         * Pick Table
        */
        if($service->parent->code == 'permit-pick') {
            if($service->code == 'pick-sick') {
                $pick                          = new RequestPickSickModels;
                $pick->item_id                 = $requestItem->id;
                $pick->scheduled_at            = Carbon::parse(post('appointment'))->format('Y-m-d H:i:s');
                $pick->origin_regency_id       = post('origin_regency_id');
                $pick->origin_district_id      = post('origin_district_id');
                $pick->origin_village_id       = post('origin_village_id');
                $pick->origin_rt               = post('origin_rt');
                $pick->origin_rw               = post('origin_rw');
                $pick->origin_address          = post('origin_address');
                $pick->destination_regency_id  = post('destination_regency_id');
                $pick->destination_district_id = post('destination_district_id');
                $pick->destination_village_id  = post('destination_village_id');
                $pick->destination_rt          = post('destination_rt');
                $pick->destination_rw          = post('destination_rw');
                $pick->destination_address     = post('destination_address');
                $pick->save();

                $requestItem->requestable_id = $pick->id;
                $requestItem->save();
            }
        }

        /**
         * If reference
         */
        if(input('ref')) {
            $permitManager->makeQueue($requestItem->id, input('ref'));
        }

        /**
         * Request Customer
         * @var RequestCustomer
         */
        $permitManager->makeRequester($requestItem->id, post());

        /**
         * Request Officer
         * @var RequestOfficer
         */
        // $permitManager->makeOfficer($requestItem->id, post());

        Flash::success('Permohonan berhasil disimpan');
        return Redirect::to('permohonan/item/detail/'.$requestItem->parameter);
    }

    public function onDelete()
    {
        $request             = RequestItemModels::whereParameter(post('parameter'))->first();
        $request->is_display = 0;
        $request->save();

        \Flash::success('Permohonan berhasil dihapus');
        return \Redirect::refresh();
    }

    public function onAddOfficer()
    {
        return [
            'officer' => $this->renderPartial('request/form-officer-item', [
                'key'      => post('length'),
                'officers' => $this->getOfficer()
            ])
        ];
    }

    public function onRemoveOfficer()
    {
        $expertise = SpeakerSpeakerExpertise::whereParameter(post('parameter'))->first()->delete();
        return [
            'expertise' => $this->renderPartial('speaker/team-list-expertises', [
                'speakerExpertises'  => $this->getSpeakerExpertises()
            ]),
            'message' => 'Keahlian berhasil dihapus',
            'type' => 'success',
        ];
    }

    public function onSelectService()
    {
        $service = ServiceModels::whereId(post('service_id'))->first();

        if(post('service_id')) {
            $this->page['serviceRequest'] = $service;
            $this->page['service']        = $service->parent;
            $this->page['officers']       = $this->getOfficer();

            if($service->code == 'permit-hearse') {
                $this->page['origin_regencies']      = Regency::orderBy('name', 'asc')->whereProvinceId(62)->get();
                $this->page['destination_regencies'] = Regency::orderBy('name', 'asc')->whereProvinceId(62)->get();
            }
        }
    }
}
