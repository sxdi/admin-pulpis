<?php namespace Web\Admin\Components;

use Cms\Classes\ComponentBase;

use Pulangpisau\Core\Classes\SessionManager;

use Pulangpisau\Request\Models\Request as RequestModels;
use Pulangpisau\Request\Models\RequestFeedback as RequestFeedbackModels;

class AdminFeedback extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminFeedback Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }

    public function getAll()
    {
        return RequestFeedbackModels::orderBy('created_at', 'desc')->get();
    }

    public function getAllFeedback()
    {
        $user     = $this->getUser();
        $services = [];

        foreach ($user->services as $service) {
            foreach ($service->service->childs as $child) {
                array_push($services, $child->id);
            }
        }

        return RequestFeedbackModels::orderBy('created_at', 'desc')->whereHas('item', function($q) use($services) {
          $q->whereIn('service_id', $services);
        })->get();
        // return RequestModels::orderBy('created_at', 'desc')->whereHas('items', function($q) use($services){
        //     $q->whereIn('service_id', $services);
        // })->get();
    }
}
