<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Core\Classes\SessionManager;
use Pulangpisau\Core\Classes\RequestPermitManager;

use Pulangpisau\Location\Models\Province;
use Pulangpisau\Location\Models\Regency;
use Pulangpisau\Location\Models\District;
use Pulangpisau\Location\Models\Village;

use Pulangpisau\Service\Models\Service as ServiceModels;

use Pulangpisau\Request\Models\Request as RequestModels;
use Pulangpisau\Request\Models\RequestItem as RequestItemModels;

use Pulangpisau\Request\Models\RequestCrowdPermit;

class AdminRequestPermitCrowd extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminRequestPermitCrowd Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }

    public function getService($id)
    {
        return ServiceModels::whereId($id)->first();
    }

    public function onSave()
    {
        $user          = $this->getUser();
        $service       = $this->getService(post('service_id'));
        $permitManager = new RequestPermitManager();

        $rules = [
            'name'                 => 'required',
            'phone'                => 'required',
            'email'                => 'email',

            'activity_type'        => 'required',
            'activity_name'        => 'required',
            'activity_purpose'     => 'required',
            'activity_participant' => 'required',
            'activity_location'    => 'required',
            'pic_name'             => 'required',
            'pic_phone'            => 'required',
            'pic_email'            => 'email',
        ];
        $messages       = [];
        $attributeNames = [
            'name'                 => 'nama',
            'phone'                => 'no telepon',
            'email'                => 'email',

            'activity_type'        => 'jenis kegiatan',
            'activity_name'        => 'nama',
            'activity_purpose'     => 'tujuan',
            'activity_participant' => 'peserta',
            'activity_location'    => 'lokasi',
            'pic_name'             => 'nama penanggung jawab',
            'pic_phone'            => 'telepon penanggung jawab',
            'pic_email'            => 'email penanggung jawab',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        /**
         * Request
         * @var [type]
         */
        $request = RequestModels::firstOrCreate([
            'code' => input('origin_code')
        ]);
        $request->save();

        /**
         * Request Item
         * @var RequestItemModels
         */
        $requestItem                   = new RequestItemModels;
        $requestItem->request_id       = $request->id;
        $requestItem->requestable_type = $service->parent->model;
        $requestItem->service_id       = $service->id;
        $requestItem->service_name     = $service->name;
        $requestItem->user_id          = $user ? $user->id : '';
        $requestItem->status           = 'progress';
        $requestItem->save();

        /**
         * Request Permit
         * @var RequestCrowdPermit
         */
        $permit              = new RequestCrowdPermit;
        $permit->item_id     = $requestItem->id;
        $permit->type        = post('activity_type');
        $permit->name        = post('activity_name');
        $permit->purpose     = post('activity_purpose');
        $permit->participant = post('activity_participant');
        $permit->location    = post('activity_location');
        $permit->pic_name    = post('pic_name');
        $permit->pic_phone   = post('pic_phone');
        $permit->pic_email   = post('pic_email');
        $permit->save();

        $requestItem->requestable_id = $permit->id;
        $requestItem->save();

        /**
         * If reference
         */
        if(input('ref')) {
            $permitManager->makeQueue($requestItem->id, input('ref'));
        }

        /**
         * Request Customer
         * @var RequestCustomer
         */
        $permitManager->makeRequester($requestItem->id, post());

        /**
         * Request Officer
         * @var RequestOfficer
         */
        $permitManager->makeOfficer($requestItem->id, post());

        Flash::success('Permohonan berhasil disimpan');
        return Redirect::to('permohonan/item/detail/'.$requestItem->parameter);
    }
}
