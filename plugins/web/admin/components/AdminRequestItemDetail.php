<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Core\Classes\SessionManager;
use Pulangpisau\Core\Classes\RequestPermitManager;

use Pulangpisau\Queue\Models\Queue as QueueModels;

use Pulangpisau\Request\Models\RequestItem as RequestItemModels;

class AdminRequestItemDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminRequestItemDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'Param'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
		$item               = $this->getCurrent();

        if(input('ref')) {
            QueueModels::whereId($item->queue->queue_id)->update([
                'status' => 'finish'
            ]);
        }

		$this->page->title  = 'Permohonan : '.$item->request->code;
		$this->page['item'] = $item;
    }

    public function getUser()
    {
        $session = new SessionManager();
        return $session->get();
    }

    public function getCurrent()
    {
        return RequestItemModels::whereParameter($this->property('parameter'))->first();
    }

    public function onSave()
    {
        $requestItem   = $this->getCurrent();
        $permitManager = new RequestPermitManager();

        $rules = [
            'name'        => 'required',
            'phone'       => 'required',
            'email'       => 'email',
            'gender'      => 'required|in:male,female',
            'job'         => 'required',
            'nationality' => 'required',
            'address'     => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'name'        => 'nama',
            'phone'       => 'no telepon',
            'email'       => 'email',
            'gender'      => 'jenis kelamin',
            'job'         => 'pekerjaan',
            'nationality' => 'kebangsaan',
            'address'     => 'alamat',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        /**
         * If reference
         */
        if(input('ref')) {
            $requestItem->status = 'progress';
            $requestItem->save();
            $permitManager->makeQueue($requestItem->id, input('ref'));
        }

        /**
         * Request Customer
         * @var RequestCustomer
         */
        $permitManager->makeRequester($requestItem->id, post());

        /**
         * Request Officer
         * @var RequestOfficer
         */
        $permitManager->makeOfficer($requestItem->id, post());

        Flash::success('Permohonan berhasil disimpan');
        return Redirect::refresh();
    }

    public function onFinish()
    {
        $permitManager = new RequestPermitManager();
        $user          = $this->getUser();
        $item          = $this->getCurrent();
        $item->status  = 'finish';
        $item->user_id = $user->id;
        $item->save();

        /**
         * If reference
        */
        if(input('ref')) {
            $permitManager->makeQueue($item->id, input('ref'));
        }

        Flash::success('Permohonan berhasil disimpan');
        return Redirect::refresh();
    }
}
