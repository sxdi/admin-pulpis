<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Service\Models\ServiceItem as ServiceItemModels;

class AdminServiceItemDetail extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminServiceItemDetail Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [
            'parameter' => [
                'name'        => 'Parameter',
                'description' => 'No description provided yet...'
            ]
        ];
    }

    public function onRun()
    {
        $item = $this->getCurrent();
        if(!$item) {

        }

        $this->page['item'] = $item;
    }

    public function init()
    {
        $item    = $this->getCurrent();
        $pdf     = $this->addComponent(
            'Responsiv\Uploader\Components\FileUploader',
            'pdfUploader',
            [
                'deferredBinding' => true,
                'maxSize'         => '2',
                'fileTypes'       => 'pdf'
            ]
        );
        $pdf->bindModel('pdf', ServiceItemModels::find($item->id));

        $video     = $this->addComponent(
            'Responsiv\Uploader\Components\FileUploader',
            'videoUploader',
            [
                'deferredBinding' => true,
                'maxSize'         => '2',
                'fileTypes'       => 'mp4'
            ]
        );
        $video->bindModel('video', ServiceItemModels::find($item->id));
    }

    public function getCurrent()
    {
        return ServiceItemModels::whereParameter($this->property('parameter'))->first();
    }

    public function onSave()
    {
        $rules = [
            'service_id' => 'required',
            'name'       => 'required',
        ];
        $messages       = [];
        $attributeNames = [
            'service_id' => 'layanan',
            'name'       => 'nama',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $serviceItem             = $this->getCurrent();
        $serviceItem->service_id = post('service_id');
        $serviceItem->name       = post('name');
        $serviceItem->code       = strtolower(post('name'));
        $serviceItem->save(null, post('_session_key'));

        Flash::success('Item berhasil disimpan');
        return Redirect::refresh();
    }
}
