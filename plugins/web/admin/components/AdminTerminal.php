<?php namespace Web\Admin\Components;

use Flash;
use Redirect;
use Validator;

use Cms\Classes\ComponentBase;

use Pulangpisau\Counter\Models\Counter as CounterModels;
use Pulangpisau\Counter\Models\Terminal as TerminalModels;

class AdminTerminal extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'AdminTerminal Component',
            'description' => 'No description provided yet...'
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function getAll()
    {
        return TerminalModels::get();
    }

    public function getCounter()
    {
        return CounterModels::orderBy('name', 'asc')->get();
    }


    /**
     * Action
    */
    public function onSave()
    {
        $rules = [
            'counter_id' => 'required',
            'number'     => 'required|numeric|min:1|max:10|unique:pulangpisau_counter_terminals,counter_id,number',
        ];
        $messages       = [];
        $attributeNames = [
            'counter_id' => 'loket',
            'number'     => 'nomor',
        ];

        $validator = Validator::make(post(), $rules, $messages, $attributeNames);
        if ($validator->fails()) {
            Flash::error($validator->messages()->first());
            return;
        }

        $terminal             = new TerminalModels;
        $terminal->counter_id = post('counter_id');
        $terminal->number     = post('number');
        $terminal->save();

        Flash::success('Terminal berhasil disimpan');
        return Redirect::refresh();
    }
}
