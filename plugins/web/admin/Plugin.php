<?php namespace Web\Admin;

use Backend;
use System\Classes\PluginBase;

/**
 * Admin Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'Admin',
            'description' => 'No description provided yet...',
            'author'      => 'Web',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        // return []; // Remove this line to activate

        return [
            'Web\Admin\Components\AdminDashboard'             => 'AdminDashboard',

            'Web\Admin\Components\AdminDetail'                => 'AdminDetail',
            'Web\Admin\Components\AdminProfile'               => 'AdminProfile',
            'Web\Admin\Components\AdminPassword'              => 'AdminPassword',

            'Web\Admin\Components\AdminFeedback'              => 'AdminFeedback',
            'Web\Admin\Components\AdminFeedbackDetail'        => 'AdminFeedbackDetail',
            'Web\Admin\Components\AdminFeedbackFile'          => 'AdminFeedbackFile',

            'Web\Admin\Components\AdminRequest'               => 'AdminRequest',
            'Web\Admin\Components\AdminRequestDetail'         => 'AdminRequestDetail',
            'Web\Admin\Components\AdminRequestItemDetail'     => 'AdminRequestItemDetail',
            'Web\Admin\Components\AdminRequestItemEdit'       => 'AdminRequestItemEdit',
            'Web\Admin\Components\AdminRequestFile'           => 'AdminRequestFile',
            'Web\Admin\Components\AdminRequestResume'         => 'AdminRequestResume',

            'Web\Admin\Components\AdminRequestPermitSim'      => 'AdminRequestPermitSim',
            'Web\Admin\Components\AdminRequestPermitSkck'     => 'AdminRequestPermitSkck',
            'Web\Admin\Components\AdminRequestPermitTnkb'     => 'AdminRequestPermitTnkb',
            'Web\Admin\Components\AdminRequestPermitDrug'     => 'AdminRequestPermitDrug',
            'Web\Admin\Components\AdminRequestPermitCrowd'    => 'AdminRequestPermitCrowd',
            'Web\Admin\Components\AdminRequestPermitHearse'   => 'AdminRequestPermitHearse',
            'Web\Admin\Components\AdminRequestPermitActivity' => 'AdminRequestPermitActivity',

            'Web\Admin\Components\AdminService'               => 'AdminService',
            'Web\Admin\Components\AdminServiceDetail'         => 'AdminServiceDetail',
            'Web\Admin\Components\AdminServiceItem'           => 'AdminServiceItem',
            'Web\Admin\Components\AdminServiceItemDetail'     => 'AdminServiceItemDetail',
            'Web\Admin\Components\AdminServiceRequest'        => 'AdminServiceRequest',
            'Web\Admin\Components\AdminServiceRequestDetail'  => 'AdminServiceRequestDetail',

            'Web\Admin\Components\AdminSector'                => 'AdminSector',
            'Web\Admin\Components\AdminSectorDetail'          => 'AdminSectorDetail',

            'Web\Admin\Components\AdminCounter'               => 'AdminCounter',
            'Web\Admin\Components\AdminCounterDetail'         => 'AdminCounterDetail',

            'Web\Admin\Components\AdminTerminal'              => 'AdminTerminal',
            'Web\Admin\Components\AdminTerminalDetail'        => 'AdminTerminalDetail',

            'Web\Admin\Components\AdminOfficer'               => 'AdminOfficer',
            'Web\Admin\Components\AdminOfficerDetail'         => 'AdminOfficerDetail',
            'Web\Admin\Components\AdminOfficerAccess'         => 'AdminOfficerAccess',

            'Web\Admin\Components\AdminUnit'                  => 'AdminUnit',
            'Web\Admin\Components\AdminUnitDetail'            => 'AdminUnitDetail',

            'Web\Admin\Components\AdminSection'               => 'AdminSection',
            'Web\Admin\Components\AdminSectionDetail'         => 'AdminSectionDetail',

            'Web\Admin\Components\AdminQueue'                 => 'AdminQueue',
            'Web\Admin\Components\AdminQueueDetail'           => 'AdminQueueDetail',
            'Web\Admin\Components\QueueMachine'               => 'QueueMachine',
            'Web\Admin\Components\QueueMachineDetail'         => 'QueueMachineDetail',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'web.admin.some_permission' => [
                'tab' => 'Admin',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return []; // Remove this line to activate

        return [
            'admin' => [
                'label'       => 'Admin',
                'url'         => Backend::url('web/admin/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['web.admin.*'],
                'order'       => 500,
            ],
        ];
    }
}
